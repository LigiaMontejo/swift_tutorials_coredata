//
//  List.m
//  GroceryList
//
//  Created by Ligia Montejo on 6/18/15.
//  Copyright (c) 2015 MobileAndCloudCo. All rights reserved.
//

#import "List.h"


@implementation List

@dynamic item;
@dynamic note;
@dynamic qty;

@end
