//
//  List.h
//  GroceryList
//
//  Created by Ligia Montejo on 6/18/15.
//  Copyright (c) 2015 MobileAndCloudCo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface List : NSManagedObject

@property (nonatomic, retain) NSString * item;
@property (nonatomic, retain) NSString * note;
@property (nonatomic, retain) NSString * qty;

@end
