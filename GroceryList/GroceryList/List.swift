//
//  List.swift
//  GroceryList
//
//  Created by Ligia Montejo on 6/18/15.
//  Copyright (c) 2015 MobileAndCloudCo. All rights reserved.
//

import Foundation
import CoreData

@objc(List)

class List: NSManagedObject {

    @NSManaged var item: String
    @NSManaged var note: String
    @NSManaged var qty: String

}
